package com.fsquiroz.fsauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Login")
@ToString(exclude = "password")
public class MLogin {

    @ApiModelProperty(example = "some@email.com")
    private String platformIdentifier;

    @ApiModelProperty(example = "some-password123")
    private String password;

    @ApiModelProperty(example = "abc_123")
    private String token;

}
