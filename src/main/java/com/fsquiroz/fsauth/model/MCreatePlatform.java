package com.fsquiroz.fsauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CreatePlatform")
public class MCreatePlatform {

    @ApiModelProperty(example = "Platform Name")
    private String name;

    @ApiModelProperty(example = "Platform simple description")
    private String description;

    @ApiModelProperty(example = "some@email.com")
    private String platformIdentifier;

    @ApiModelProperty(example = "some-password")
    private String password;

}
