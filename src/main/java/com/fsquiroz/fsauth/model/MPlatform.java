package com.fsquiroz.fsauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Platform")
public class MPlatform {

    @ApiModelProperty(example = "5c10173a82108a5ab5866d65")
    private String id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "Platform Name")
    private String name;

    @ApiModelProperty(example = "Platform simple description")
    private String description;

}
