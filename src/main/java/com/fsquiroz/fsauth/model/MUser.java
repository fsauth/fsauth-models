package com.fsquiroz.fsauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User")
@ToString(exclude = "password")
public class MUser {

    @ApiModelProperty(example = "5c10173a82108a5ab5866d65")
    private String id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "USER")
    private String role;

    @ApiModelProperty(example = "some@email.com")
    private String platformIdentifier;

    @ApiModelProperty(example = "some-password")
    private String password;

    private Map<String, Object> attributes;

    @ApiModelProperty(hidden = true)
    private MPlatform platform;

}
