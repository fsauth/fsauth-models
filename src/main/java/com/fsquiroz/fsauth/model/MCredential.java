package com.fsquiroz.fsauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Credential")
public class MCredential {

    @ApiModelProperty(example = "5c10173a82108a5ab5866d65")
    private String id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    private MUser user;

    private MUser issuer;

    @ApiModelProperty(hidden = true)
    private Instant issuedAt;

    @ApiModelProperty(hidden = true)
    private Instant expiration;

    @ApiModelProperty(hidden = true)
    private Instant notBefore;

    @ApiModelProperty(hidden = true)
    private String token;

}
